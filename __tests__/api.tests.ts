
const request = require("supertest")("https://api.stlouisfed.org/fred/releases?api_key=902dba4471e89b90af2f439c996933aa&file_type=json");


describe("Response test", function () {
  it("should have status code 200", async function () {
    const response = await request.get("");
    expect(response.status).toEqual(200);
    
  });
});
